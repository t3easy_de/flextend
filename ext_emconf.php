<?php

########################################################################
# Extension Manager/Repository config file for ext: "flextend"
#
# Auto generated by Extension Builder 2012-09-26
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Fluid/Extbase additions',
	'description' => 'Additional ViewHelpers
Services, Utilities',
	'category' => 'misc',
	'author' => 'Jan Kiesewetter',
	'author_email' => 'janYYYY@t3easy.de',
	'author_company' => 't3easy',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '0.0.3',
	'constraints' => array(
		'depends' => array(
			'extbase' => '1.3',
			'fluid' => '1.3',
			'typo3' => '4.5',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>