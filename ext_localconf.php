<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// include required TypoScript Setup always after cssstyledcontent/static/ key
$requiredTypoScript = '
plugin.tx_flextend.settings.defaultHeaderType = {$content.defaultHeaderType}
';
t3lib_extMgm::addTypoScript('flextend', 'setup', $requiredTypoScript, 'cssstyledcontent/static/');

?>