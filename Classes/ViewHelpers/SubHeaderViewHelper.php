<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package flextend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Flextend_ViewHelpers_SubHeaderViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractTagBasedViewHelper {

	/**
	 * @var string $tagName
	 */
	protected $tagName;

	/**
	 *
	 * @var Tx_Extbase_Configuration_ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * Initialize arguments
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerArgument('increase', 'integer', 'increase');
	}

	/**
	 *
	 * @return void initialize the ViewHelper
	 */
	public function initialize() {
		parent::initialize();
		$this->tag->setTagName($this->setTagName());
	}

	/**
	 *
	 * @param Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager
	 * @return void
	 */
	public function injectConfigurationManager(Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
	}

	/**
	 * Render the header tag
	 *
	 * @return string rendered tag
	 */
	public function render() {
		$this->tag->setContent($this->renderChildren());
		return $this->tag->render();
	}

	/**
	 *
	 * @return string $tagName
	 */
	public function setTagName() {
		$settings = $this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,'flextend', 'Pi1');
		$defaultHeaderType = intval($settings['defaultHeaderType']);

		if ($this->hasArgument('increase')) {
			$increase = intval($this->arguments['increase']);
		} else {
			$increase = 1;
		}
		$subHeaderTypeCalc = $defaultHeaderType + $increase;

		$subHeaderType = ($subHeaderTypeCalc <= 6) ? $subHeaderTypeCalc : 6;

		$tagName = 'h'.$subHeaderType;
		return $tagName;
	}
}

?>