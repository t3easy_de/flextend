<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Switch ViewHelper
 *
 * @package flextend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Flextend_ViewHelpers_SwitchViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractConditionViewHelper {

	/**
	 * An array of Tx_Fluid_Core_Parser_SyntaxTree_AbstractNode
	 *
	 * @var array
	 */
	private $childNodes = array();

	/**
	 * Setter for ChildNodes - as defined in ChildNodeAccessInterface
	 *
	 * @param array $childNodes Child nodes of this syntax tree node
	 * @return void
	 */
	public function setChildNodes(array $childNodes) {
		$this->childNodes = $childNodes;
	}

	/**
	 * initialize the arguments
	 */
	public function initializeArguments() {
		$this->registerArgument('value', 'string', 'The switch value to compare the cases.', TRUE);
		$this->registerArgument('breakFirstCase', 'boolean', 'Break at first matched case.', FALSE, TRUE);
	}

	/**
	 * Render
	 *
	 * @return string the rendered string
	 */
	public function render() {
		$value = $this->arguments['value'];
		$this->viewHelperVariableContainer->addOrUpdate('Tx_Flextend_ViewHelpers_SwitchViewHelper', 'value', $value);
		$this->viewHelperVariableContainer->addOrUpdate('Tx_Flextend_ViewHelpers_SwitchViewHelper', 'caseMatched', FALSE);
		$this->viewHelperVariableContainer->addOrUpdate('Tx_Flextend_ViewHelpers_SwitchViewHelper', 'breakFirstCase', $this->arguments['breakFirstCase']);
			//TODO: remove tags other than case or default
		return $this->renderChildren();
	}

}

?>