<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package flextend
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Flextend_ViewHelpers_GravatarViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractTagBasedViewHelper {

	/**
	 * The tag name
	 *
	 * @var string $tagName
	 */
	protected $tagName = 'img';

	/**
	 * Initialize arguments
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerTagAttribute('alt', 'string', 'Specifies an alternate text for an image',TRUE);
		$this->registerArgument('email', 'string', 'increase');
		$this->registerArgument('hash', 'string', 'Hash');
		$this->registerArgument('forceHttps', 'bool', 'Force the image from https.');
		$this->registerArgument('size', 'int', 'Size since the images are square. Default 80px by 80px. See https://de.gravatar.com/site/implement/images/#size');
		$this->registerArgument('default', 'string', 'URL of your default image or one of the keywords: 404, mm, identicon, monsterid, wavatar,retro,blank. See https://de.gravatar.com/site/implement/images/#default-image');
		$this->registerArgument('forcedefault', 'bool', 'If for some reason you wanted to force the default image to always load. See https://de.gravatar.com/site/implement/images/#force-default');
		$this->registerArgument('rating', 'string', 'Request images up to and including that rating: g, pg, r, x. See https://de.gravatar.com/site/implement/images/#rating');
	}

	/**
	 * Render the image tag
	 *
	 * @return string rendered tag
	 */
	public function render() {
		$settings = array();
		if ($_SERVER['HTTPS'] || $this->arguments['forceHttps']) {
			$srcBase = 'https://secure.gravatar.com/avatar/';
		} else {
			$srcBase = 'http://www.gravatar.com/avatar/';
		}
		if ($this->arguments['hash']) {
			$src = $srcBase . $this->arguments['hash'];
		} elseif ($this->arguments['email']) {
			$hash = md5(strtolower(trim($this->arguments['email'])));
			$src = $srcBase . $hash;
		} else {
			return 'hash or email missing';
		}
		if ($this->arguments['size']) {
			$settings[] = 's=' . $this->arguments['size'];
			$this->tag->addAttribute('width', $this->arguments['size']);
			$this->tag->addAttribute('height', $this->arguments['size']);
		} else {
			$this->tag->addAttribute('width', '80');
			$this->tag->addAttribute('height', '80');
		}
		if ($this->arguments['default']) {
			$settings[] = 'd=' . $this->arguments['default'];
		}
		if ($this->arguments['rating']) {
			$settings[] = 'r=' . $this->arguments['rating'];
		}
		if ($this->arguments['forcedefault'] === TRUE) {
			$settings[] = 'f=y';
		}
		if (!empty($settings)) {
			$src .= '?' . implode('&',$settings);
		}
		if ($this->arguments['title'] === '') {
			$this->tag->addAttribute('title', $this->arguments['alt']);
		}
		$this->tag->addAttribute('src', $src);
		return $this->tag->render();
	}
}

?>