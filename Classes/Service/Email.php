<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
= Examples =
$mail = $this->objectManager->get('Tx_Flextend_Service_Email');

$mail->setControllerName($this->request->getControllerName());
$mail->setTemplateName('Email');

$mailIsSend = $mail->send($to, $subject, array('model' => $model), TRUE, TRUE);

if ($mailIsSend === TRUE) {
	....
}
 *
 */

/**
 *
 *
 * @package flextend
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Flextend_Service_Email implements t3lib_Singleton {

	/**
	 *
	 * @var array configuration
	 */
	protected $configuration;

	/**
	 *
	 * @var string controller name
	 */
	protected $controllerName;

	/**
	 *
	 * @var string template name
	 */
	protected $templateName;

	/**
	 *
	 * @param string $controllerName
	 */
	public function setControllerName($controllerName) {
		$this->controllerName = $controllerName;
	}

	/**
	 *
	 * @param string $templateName
	 */
	public function setTemplateName($templateName = 'Email') {
		$this->templateName = $templateName;
	}

	/**
	 *
	 * @var Tx_Extbase_Configuration_ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * @var Tx_Extbase_Object_ObjectManagerInterface
	 */
	protected $objectManager;

	/**
	 * inject configuration manager
	 *
	 * @param Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager
	 */
	public function injectConfigurationManager(Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
	}

	/**
	 * inject object manager
	 * @param Tx_Extbase_Object_ObjectManagerInterface $objectManager
	 */
	public function injectObjectManager(Tx_Extbase_Object_ObjectManagerInterface $objectManager) {
		$this->objectManager = $objectManager;
	}

	/**
	 * Send the e-mail
	 *
	 * @param array|string $to
	 * @param string $subject
	 * @param array $variables
	 * @param bool $html
	 * @param bool $txt
	 * @param array|string $from
	 * @return bool
	 */
	public function send($to, $subject, $variables, $html = TRUE, $txt = FALSE, $from = '') {
		$this->configuration = $this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);

		$message = t3lib_div::makeInstance('t3lib_mail_Message');

		if (!is_array($to)) {
			$to = array($to => $to);
		}

		$message->setTo($to)
			->setSubject($subject);

		if ($txt) {
			$message->addPart($this->getRenderedView($variables, 'txt'), 'text/plain');
		}
		if ($html) {
			$message->addPart($this->getRenderedView($variables), 'text/html');
		}

		if(!empty($from)) {
			if (!is_array($from)) {
				$from = array($from => $from);
			}
		} else {
			$from = t3lib_utility_Mail::getSystemFrom();
		}

		$message->setFrom($from);

		$message->send();
		return $message->isSent();
	}

	/**
	 * @param array $variables
	 * @param string $format
	 * @return string
	 */
	protected function getRenderedView($variables, $format = 'html'){
		$view = $this->objectManager->get('Tx_Fluid_View_StandaloneView');
		$view->getRequest()->setControllerExtensionName($this->configuration['extensionName']);
		$view->getRequest()->setPluginName($this->configuration['pluginName']);
		$view->getRequest()->setControllerName($this->controllerName);
		#$view->getRequest()->setControllerActionName($this->templateName);

		$view->setFormat($format);
		$view->setTemplatePathAndFilename($this->getTemplatePathAndFilename($format));
		$view->setLayoutRootPath($this->getLayoutRootPath());
		$view->setPartialRootPath($this->getPartialRootPath());
		$view->assignMultiple($variables);

		return $view->render();
	}

	/**
	 * @param string $format
	 * @return string Absolute template file path
	 */
	protected function getTemplatePathAndFilename($format = 'html') {
		$templateRootPath = t3lib_div::getFileAbsFileName($this->configuration['view']['templateRootPath']);
		$templatePathAndFilename = $templateRootPath . $this->controllerName . '/' . $this->templateName . '.' . $format;
		return $templatePathAndFilename;
	}

	/**
	 *
	 * @return string absolute layout root path
	 */
	protected function getLayoutRootPath() {
		$layoutRootPath = t3lib_div::getFileAbsFileName($this->configuration['view']['layoutRootPath']);
		return $layoutRootPath;
	}

	/**
	 *
	 * @return string absolute patrial root path
	 */
	protected function getPartialRootPath() {
		$partialRootPath = t3lib_div::getFileAbsFileName($this->configuration['view']['partialRootPath']);
		return $partialRootPath;
	}
}

?>