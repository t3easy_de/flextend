<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Add resource Service
 *
 * @package flextend
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_Flextend_Service_AddResources implements t3lib_Singleton {

	/**
	 * Adds javascript library from a cached action
	 *
	 * @param string $name Arbitrary identifier
	 * @param string $file File name
	 * @param boolean $footer
	 * @param string $type Content Type
	 * @param boolean $compress		flag if library should be compressed
	 * @param boolean $forceOnTop	flag if added library should be inserted at begin of this block
	 * @param string $allWrap
	 * @param boolean $excludeFromConcatenation
	 * @return void
	 */
	public static function addJsLibrary($name, $file, $footer = TRUE, $type= 'text/javascript', $compress=FALSE, $forceOnTop=FALSE, $allWrap= '', $excludeFromConcatenation=FALSE){

		if ($footer === TRUE) {
			$GLOBALS['TSFE']->getPageRenderer()->addJsFooterLibrary($name, $file, $type, $compress, $forceOnTop, $allWrap, $excludeFromConcatenation);
		} else{
			$GLOBALS['TSFE']->getPageRenderer()->addJsLibrary($name, $file, $type, $compress, $forceOnTop, $allWrap, $excludeFromConcatenation);
		}
	}

	/**
	 * Adds javascript file from a cached action
	 *
	 * @param string $file File name
	 * @param boolean $footer
	 * @param string $type Content Type
	 * @param boolean $compress		flag if library should be compressed
	 * @param boolean $forceOnTop	flag if added library should be inserted at begin of this block
	 * @param string $allWrap
	 * @param boolean $excludeFromConcatenation
	 * @return void
	 */
	public static function addJsFile($file, $footer=TRUE, $type= 'text/javascript', $compress=TRUE, $forceOnTop=FALSE, $allWrap= '', $excludeFromConcatenation=FALSE){

		if (strpos($file, 'ajax.php?') !== FALSE || strpos($file, 'eID=') !== FALSE ) {
			$compress = FALSE;
		}

		if ($footer === TRUE) {
			$GLOBALS['TSFE']->getPageRenderer()->addJsFooterFile($file, $type, $compress, $forceOnTop, $allWrap, $excludeFromConcatenation);
		} else{
			$GLOBALS['TSFE']->getPageRenderer()->addJsFile($file, $type, $compress, $forceOnTop, $allWrap, $excludeFromConcatenation);
		}
	}

	/**
	 * Add css file from a cached action
	 *
	 * @param $file
	 * @param string $rel
	 * @param string $media
	 * @param string $title
	 * @param bool $compress
	 * @param bool $forceOnTop
	 * @param string $allWrap
	 * @param bool $excludeFromConcatenation
	 */
	public static function css($file, $rel= 'stylesheet', $media= 'all', $title= '', $compress=TRUE, $forceOnTop=FALSE, $allWrap= '', $excludeFromConcatenation=FALSE) {
		$GLOBALS['TSFE']->getPageRenderer()->addCssFile($file, $rel, $media, $title, $compress, $forceOnTop, $allWrap, $excludeFromConcatenation);
	}
}

?>