<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Md5 Validator
 *
 * @package flextend
 * @subpackage Validation\Validator
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Flextend_Validation_Validator_Md5Validator extends Tx_Extbase_Validation_Validator_AbstractValidator {

	/**
	 * isValid
	 *
	 * @param string $value The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occurred
	 */
	public function isValid($value) {
		$null = 'd41d8cd98f00b204e9800998ecf8427e';
		if (strlen($value) !== 32 && !ctype_xdigit($value)){
			$this->addError('The given subject was not a valid md5 hash.', 1353922190);
			return FALSE;
		} elseif (isset($this->options['nullHashIsFalse']) && $this->options['nullHashIsFalse'] == TRUE && $value === $null) {
			$this->addError('The given subject hash was from null.', 1353922227 );
			return FALSE;
		} else{
			return TRUE;
		}
	}
}

?>