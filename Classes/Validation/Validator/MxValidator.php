<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Validator for domain mx records of email addresses
 *
 * @package flextend
 * @subpackage Validation\Validator
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Flextend_Validation_Validator_MxValidator extends Tx_Extbase_Validation_Validator_AbstractValidator {

	/**
	 *
	 * @var Tx_Extbase_Configuration_ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 *
	 * @param Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager
	 * @return void
	 */
	public function injectConfigurationManager(Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
	}

	/**
	 * Checks if the domain of the given email address has a mx record.
	 * This validator should be used in combination with the email address validator
	 *
	 * @param string $value The value that should be validated
	 * @return boolean TRUE if the value is valid, FALSE if an error occurred
	 */
	public function isValid($value) {
		$settings = $this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
		if ($settings['flextend']['disableMxValidator']) {
			return TRUE;
		} else {
			list ($user, $domain) = explode('@',$value);
			if (checkdnsrr($domain,'MX')) {
				return TRUE;
			} else {
				$this->addError('No MX record found for domain "' . $domain . '".' , 1354188212, array($domain));
				return FALSE;
			}
		}
	}
}

?>